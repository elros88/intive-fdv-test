package com.example.randomuser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.randomuser.model.Result;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

public class UserDetailActivity extends AppCompatActivity {

    ImageView userPhoto;
    TextView username;
    TextView name;
    TextView lastname;
    TextView email;
    Result user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        userPhoto = findViewById(R.id.userPhoto);
        username = findViewById(R.id.username);
        name = findViewById(R.id.name);
        lastname = findViewById(R.id.last_name);
        email = findViewById(R.id.email);
        user = new Result();

        Bundle bundle = getIntent().getExtras();
        String json = bundle.getString("USER", null);
        if(json != null)
        {
            Gson gson = new Gson();
            user =  gson.fromJson(json, Result.class);
            setView();
        }
    }

    void setView(){
        Picasso.get().load(user.getPicture().getLarge()).into(userPhoto);
        username.setText(user.getLogin().getUsername());
        name.setText(user.getName().getFirst());
        lastname.setText(user.getName().getLast());
        email.setText(user.getEmail());
    }
}
