package com.example.randomuser;

import android.os.Bundle;

import com.example.randomuser.adapters.UserListAdapter;
import com.example.randomuser.model.Results;
import com.example.randomuser.restservice.RestInterface;
import com.example.randomuser.restservice.RestService;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView userListView;
    UserListAdapter adapter;
    Results userResults;
    ProgressBar bar;
    RestInterface restInterface;
    String TAG = "Example";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        restInterface = new RestService().getInstance();

        bar = findViewById(R.id.progressBar);

        userListView = findViewById(R.id.user_list);
        userListView.setLayoutManager(new GridLayoutManager(this, 3));

        userListView.setAdapter(adapter);

        getUsers();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.about_message)
                    .setTitle(R.string.about_title);
            AlertDialog dialog = builder.create();
            dialog.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void getUsers(){
        Call<Results> call =restInterface.getUsers();
        call.enqueue(new Callback<Results>() {
            @Override
            public void onResponse(Call<Results> call, Response<Results> response) {
                Log.d(TAG, "onResponse: " + response.body());
                userResults = response.body();
                bar.setVisibility(View.GONE);
                userListView.setVisibility(View.VISIBLE);
                UserListAdapter adapter = new UserListAdapter(MainActivity.this, userResults.getResults());
                userListView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Results> call, Throwable t) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage(R.string.error_message)
                        .setTitle(R.string.error_title);
                AlertDialog dialog = builder.create();
                dialog.show();
                bar.setVisibility(View.GONE);
            }
        });
    }
}
