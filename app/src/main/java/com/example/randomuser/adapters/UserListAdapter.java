package com.example.randomuser.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.randomuser.R;
import com.example.randomuser.UserDetailActivity;
import com.example.randomuser.model.Result;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;


public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {

    private List<Result> users;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    // data is passed into the constructor
    public UserListAdapter(Context context, List<Result> data) {
        this.mInflater = LayoutInflater.from(context);
        this.users = data;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.user_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (users != null && users.size()!=0) {
            final int positionInList = position;
            holder.userName.setText(users.get(position).getName().getFirst() + " " +
                    users.get(position).getName().getLast());

            Picasso.get().load(users.get(position).getPicture().getThumbnail()).into(holder.userPhoto);

            holder.userPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, UserDetailActivity.class);
                    Gson gson = new Gson();
                    intent.putExtra("USER", gson.toJson(users.get(positionInList)));
                    context.startActivity(intent);
                }
            });
        }

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return users.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView userName;
        ImageView userPhoto;

        ViewHolder(View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.user_name_placeholder);
            userPhoto = itemView.findViewById(R.id.user_image_place_holder);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    Result getItem(int id) {
        return users.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
