package com.example.randomuser.restservice;

import com.example.randomuser.utils.Urls;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RestService {

    RestInterface restInterface;

    public RestInterface getInstance(){
        if(restInterface == null){
            Retrofit retrofit = new Retrofit.Builder().
                    baseUrl(Urls.baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            restInterface = retrofit.create(RestInterface.class);
            return restInterface;
        }
        return restInterface;
    }
}
