package com.example.randomuser.restservice;

import com.example.randomuser.model.Results;
import com.example.randomuser.utils.Urls;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RestInterface {

    @GET(Urls.usersEndpoint)
    Call<Results> getUsers();
}
