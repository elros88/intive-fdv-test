package com.example.randomuser.utils;

public class Urls {

    public static final String baseUrl = "https://randomuser.me/api/";
    public static final String usersEndpoint = "?results=50&nat=US";
}
